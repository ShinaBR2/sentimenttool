import http from 'http';
import app from './app/app';
import { port } from './config';

const server = http.Server(app);

server.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
