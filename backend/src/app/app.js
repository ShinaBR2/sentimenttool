import express from 'express';
import bodyParser from 'body-parser';

import Sentiment from 'sentiment';
const sentiment = new Sentiment();

const app = express();

// To correctly get the request ip address if the app is served via proxy
// In nginx config we should add the following line:
// proxy_set_header X-Real-IP $remote_addr;
app.set('trust proxy', true);

app.use(bodyParser.json({
  limit: 1000000,
}));

app.post('/api/analyze', (req, res) => {
  const results = req.body.arr.map(s => sentiment.analyze(s));

  res.status(200).send({
    success: 'true',
    message: 'Analyze successfully',
    results
  });
});

export default app;
