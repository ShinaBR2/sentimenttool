## REQUIREMENTS

### Non-functional requirement(s)
- Sentiment analysis from reddit comments.
- User input company name and submit

### Functional requirement(s)
- SPA
- React

## Analyze requirements
### Non-functional requirements


### Functional requirements
- We need some constants:
    + API URLs
- We need some components:
    + Button
    + Input
    + Form
    + Analysis result
- We need some APIs:
    + Search from user input (company name)
    + Fetch comment from a specific post
    + push comments get from reddit to out backend and return sentiment analysis result.

## HOW TO RUN

- Run backend from root folder
```
cd backend
npm install
npm start
```
- Run frontend from root folder
```
cd frontend
npm install
npm start
```
