import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import './styles/main.scss';

const app = (
  <App />
);

ReactDOM.render(app, document.getElementById('root'));
