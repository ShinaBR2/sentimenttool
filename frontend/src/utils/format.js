export const formatMoney = x => {
  if (!x || Number.isNaN(x)) {
    return 0;
  }

  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};
