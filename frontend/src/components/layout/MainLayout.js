import React from 'react';
import { Layout } from 'antd';
import './MainLayout.scss';

const {
  Footer, Content,
} = Layout;

class MainLayout extends React.Component {
  render() {
    return (
      <Layout className="mainLayout">
        <Content>{this.props.children}</Content>
        <Footer style={{ textAlign: 'center' }}>
          ©2019 Created by ShinaBR2
        </Footer>
      </Layout>
    );
  }
}

export default MainLayout;