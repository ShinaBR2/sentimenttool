import React from 'react';
import { Row, Col, Spin, Icon, Empty, Card } from 'antd';
import { formatMoney } from '../../../utils/format';
import './SearchResult.scss';

class SearchResult extends React.Component {
  render() {
    const { isLoading, message, companyName, result = {} } = this.props;
    const {
      totalPositive,
      totalNegative,
      totalNeutral,
      totalComments,
    } = result;

    const positivePercent = Math.round(totalPositive / totalComments * 100);
    const negativePercent = Math.round(totalNegative / totalComments * 100);
    const neutralPercent = Math.round(totalNeutral / totalComments * 100);

    if (isLoading) {
      return (
        <div className="SearchResultWrapper">
          <Row>
            <Col span={12} offset={6} className="text-center">
              <Spin size="large" />
            </Col>
            <Col span={12} offset={6} className="text-center">
              <p>{message}</p>
            </Col>
          </Row>
        </div>
      );
    }

    if (!totalComments) {
      return (
        <div className="SearchResultWrapper">
          <Row>
            <Col span={12} offset={6} className="text-center">
              <Empty description="No comments" />
            </Col>
          </Row>
        </div>
      );
    }

    return (
      <div className="SearchResultWrapper">
        <Row>
          <Col span={12} offset={6}>
            <h3 className="text-center">Analyze result</h3>
          </Col>
        </Row>
        <Row>
          <Col span={12} offset={6}>
            <h3 className="text-center">{`${companyName.charAt(0).toUpperCase()}${companyName.slice(1)}`}</h3>
          </Col>
          <Col span={12} offset={6}>
            <p className="text-center">Base on <strong>{formatMoney(totalComments)}</strong> comments from Reddit</p>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={8} className="text-center">
            <Card>
              <Icon type="smile" theme="twoTone" style={{ fontSize: '6em'}} />
              <p>Positive</p>
              <p>
                <strong>{positivePercent}%</strong>
              </p>
            </Card>
          </Col>
          <Col span={8} className="text-center">
            <Card>
              <Icon type="frown" theme="twoTone" style={{ fontSize: '6em'}} />
              <p>
                Negative
              </p>
              <p>
                <strong>{negativePercent}%</strong>
              </p>
            </Card>
          </Col>
          <Col span={8} className="text-center">
            <Card>
              <Icon type="meh" theme="twoTone" style={{ fontSize: '6em'}} />
              <p>
                Neutral
              </p>
              <p>
                <strong>{neutralPercent}%</strong>
              </p>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default SearchResult;

