import React from 'react';
import { Input } from 'antd';
import locale from '../../../stores/locale';
import './SearchForm.scss';

const Search = Input.Search;

class SearchForm extends React.Component {
  handleSearch = value => {
    if (value) {
      return this.props.onValidSubmit(value);
    }

    return;
  };

  render() {
    return (
      <div className="SearchFormWrapper">
        <Search
          placeholder={locale.l('placeholer.searchInput')}
          size="large"
          onSearch={this.handleSearch}
          className="SearchInputWrapper"
        />
      </div>
    );
  }
}

export default SearchForm;
