import React from 'react';
import axios from 'axios';
import {
  PageHeader,
  Comment, Icon, Tooltip, Avatar, Spin,
} from 'antd';
import SearchForm from './logic/searchForm';
import SearchResult from './logic/searchResult';
import { MainLayout } from './layout';
import locale from '../stores/locale';

const domain = 'https://www.reddit.com/r';
const subReddit = `${domain}/technology`;

/**
 * t3 == link
 * t1 == comment
 */

const getSearchLinks = arr => {
  return arr.filter(item => item.kind === 't3').map(item => {
    const { data = {} } = item;
    const {
      id,
      title,
      subreddit,
      author,
      num_comments: totalComments,
      permalink: link,
    } = data;

    return {
      id,
      title,
      subreddit,
      link,
      author,
      totalComments,
    };
  });
};

const getComments = item => {
  const { id, subreddit } = item;
  const url = `${domain}/${subreddit}/comments/${id}.json`;

  return new Promise(resolve => {
    axios.get(url).then(response => {
      // handle success
      const { data = {} } = response;
      const list = data[1].data.children
        .filter(i => i.kind === 't1')
        .map(i => i.data.body);

      resolve(list);
    })
    .catch(error => {
      // handle error
      console.log(error);
      resolve([]);
    })
    .then(() => {
      // always executed
    });
  });
};

const analyzeResult = arr => {
  const totalComments = arr.length;
  let totalPositive = 0;
  let totalNegative = 0;
  let totalNeutral = 0;

  for (let i = 0; i < totalComments; i++) {
    if (arr[i].comparative > 0) {
      totalPositive = totalPositive + 1;
    } else if (arr[i].comparative < 0) {
      totalNegative = totalNegative + 1;
    } else {
      totalNeutral = totalNeutral + 1;
    }
  }

  return {
    totalPositive,
    totalNegative,
    totalNeutral,
    totalComments,
  };
};

class App extends React.Component {
  state = {
    message: '',
    companyName: '',
    formLoading: false,
    analyzing: false,
    totalPositive: 0,
    totalNegative: 0,
    totalNeutral: 0,
    totalComments: 0,
  };

  onSearch = companyName => {
    // TODO
    // Call api to fetch result
    const url = `${subReddit}/search.json?q=${companyName}`;
    this.setState({
      companyName,
      message: `Searching for company ${companyName.charAt(0).toUpperCase() + companyName.slice(1)} ...`,
      formLoading: true,
      analyzing: true,
    });

    axios.get(url)
      .then(response => {
        const { data = {} } = response;
        const results = getSearchLinks(data.data.children);

        Promise.all(
          results.map(r => getComments(r))
        ).then(allComments => {
          const arr = allComments.flat();

          this.setState({
            message: `Analyzing data ...`,
          });

          axios.post('/api/analyze', {
            arr
          }).then(({ data }) => {
            const { results: aR } = data;
            console.log(aR);
            const finalResult = analyzeResult(aR);

            this.setState({
              ...finalResult,
              message: 'Analyze finished',
              analyzing: false,
            });
          });
        });
      })
      .catch(error => {
        // handle error
        console.log(error);
      })
      .then(() => {
        // always executed
        this.setState({
          formLoading: false,
        });
      });
  };

  render() {
    const {
      message,
      companyName,
      formLoading,
      analyzing,
      totalPositive,
      totalNegative,
      totalNeutral,
      totalComments,
    } = this.state;
    const result = {
      totalPositive,
      totalNegative,
      totalNeutral,
      totalComments,
    };

    return (
      <MainLayout>
        <PageHeader title="Sentiment Analysis" />
        <SearchForm
          onValidSubmit={this.onSearch}
          loading={formLoading}
        />
        <SearchResult
          isLoading={analyzing}
          message={message}
          companyName={companyName}
          result={result}
        />
      </MainLayout>
    );
  }
}

export default App;
