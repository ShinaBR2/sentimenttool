import en from '../languages/en';

class Locale {
  state = {
    labels: en,
  };

  l = key => this.state.labels[key] || key;
}

export default new Locale();
