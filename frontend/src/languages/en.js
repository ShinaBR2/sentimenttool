export default {
  'error.Unknown': 'Unknow error has occurred',

  'placeholer.searchInput': 'Please enter company name',

  'label.submit': 'Submit',

  'formMessage.searchForm.invalidCompanyName': 'Please enter a valid company name',
};
